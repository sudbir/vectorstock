const state = {
  bLightbox: true,
  bShowLogin: false,
  bListening: false,
  bLoginPanelControl: false,
  /* */
  pluginActive_: [],
  uidhost: ''
}

const mutations = {
  showFormLogin ()   { state.bLightbox = state.bShowLogin = true, state.bLoginPanelControl = false },
  hideFormLogin ()   { state.bLightbox = true, state.bShowLogin = false },
  getFormLogin  ()   { return (state.bShowLogin && state.bLightbox )},
  showLightBox  ()   { state.bLightbox = true },
  getLightBox   ()   { return state.bLightbox },
  showPanelControl () { state.bLightbox = true, state.bShowLogin = false, state.bLoginPanelControl = true },
  hidePanelControl () { state.bShowLogin = true, state.bLoginPanelControl = false },
  setListening ()     { state.bListening = true },
  showPluginWeb ()   { state.bLightbox = state.bShowLogin = false }
}

const actions = {
  someAsyncTask ({ commit }) {
    // do something async
    commit( 'showLightBox' )
    commit( 'showFormLogin' )
    commit( 'hidePanelControl' )
    commit( 'showPanelControl' )
    commit( 'hideFormLogin' )
  }
}

export default {
  state,
  mutations,
  actions
}
