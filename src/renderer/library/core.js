
/*
 * Author: hbiblia@gmail.com - slam
 * Desc: tenemos cosas que se utilizan mucho.
 */

import Noty from 'noty'

export default {

  /*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 27/8/2018
   * Desc: getDate
   */
  getDate: function () {
    var today = new Date();
    return today.toLocaleDateString("en-US")
  },

  /*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 20/8/2018
   * Desc: notify
   */
  notify: function ( texto, type ) {
  	new Noty( {
  		theme: 'sunset',
  		text: texto,
  		timeout: 2000,
  		type: type
  	} ).show();
  },

  /*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 20/8/2018
   * Desc: loadUrl
   */
  loadUrl: function ( URL, call ) {
  	let webview = document.querySelector( 'webview' );
  	webview.stop();
  	webview.loadURL( URL );
  	this.navigate( call );
  },

  /*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 20/8/2018
   * Desc: navigate
   */
  navigate: function ( call ) {
  	let webview = document.querySelector( 'webview' );
  	webview.addEventListener( 'did-navigate', function _listener( e ) {
  		webview.removeEventListener( 'did-navigate', _listener, true );
  		webview.addEventListener( 'did-stop-loading', function _listener( e ) {
  			webview.removeEventListener( 'did-stop-loading', _listener, true );
  			call( webview );
  		}, true);
  	},true);
  },

  /*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 20/8/2018
   * Desc: exec
   */
  exec: function (code, callback) {
    let webview = document.querySelector( 'webview' );
    webview.executeJavaScript(code, callback)
  }

}
