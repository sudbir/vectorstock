
/*
 * Author: hbiblia@gmail.com - slam
 * Desc: manejamos todo lo relacionado a los plugin.
 */

import os from 'os'
import crypto from 'crypto'
import publicIp from 'public-ip'
import core from './core.js'
import historyfunc from './history'

export default {

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 20/8/2018
	 * Desc: contamos las veces que intentamos conectar.
	 * state: deprecate
	 */
	intentLogin: 0,

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 20/8/2018
	 * Desc: cantidad maxima del login.
	 * state: deprecate
	 */
	maxIntent: 3,

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 14/8/2018
	 * Desc: guardamos la url de logout.
	 */
	urlLogout: 'http://google.com',

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 22/8/2018
	 * Desc: plugin activo
	 */
	plugin:[],

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 22/8/2018
	 * Desc: tenemos el hash session
	 */
	session:'',

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 24/8/2018
	 * Desc: completedautentificacion
	 */
	completedautentificacion: false,

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 24/8/2018
	 * Desc: completedautentificacion
	 */
	queryState: null,

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 24/8/2018
	 * Desc: autoConnect
	 */
	autoConnect: function ( self, session, loginfunc ) {
		this.activeConnect(session,(r)=>{ if( r === 'completed') {
			this.startStateEvent( self, session, loginfunc.getUidHost() )
			self.$store.commit( 'showPluginWeb' )
		}else { self.$store.commit( 'showFormLogin' ) } })
	},

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 15/8/2018
	 * Desc: stateEvent
	 */
	startStateEvent: function ( self, session, uidhost ) {
		const BrowserWindow = require('electron').remote
	 	let self_ = this
		let webview = document.querySelector( 'webview' );

		 let stateCall = function() {  self_.adblock(self_.plugin) }

		 let didStartLoading = ()=> {
			 document.querySelector( '.toolbar_progress' ).style.opacity = '1'
			 self_.adblock(self_.plugin) }

		 let didStopLoading = ()=> {
			 document.querySelector( '.toolbar_progress' ).style.opacity = '0'
		 		self_.adblock(self_.plugin) }

		 let willDownload = function(event, item, webContents) {
			 core.notify('Download start','info')
			 item.on('updated', (event, state) => {
				 if( state === 'interrupted' ){}else if( state === 'progressing') {
					 if(item.isPaused()) {
					 } else {
						 console.log(`Received bytes: ${item.getReceivedBytes()}`)
					 }
				 }
			 })
			 item.once('done', (event, state) => {
				 if(state === 'completed') {
					 let url_ = webview.getURL()
					 let idImg = url_.substr(url_.lastIndexOf('&m=')+3, Infinity)
					 idImg = parseInt(idImg.split('&')[0])
					 core.notify('Download successfully','success')
					 historyfunc.save(idImg)
				 } else {
					 core.notify(`Download failed: ${state}`,'error')
				 }
			 })
		 }

			webview.removeEventListener('update-target-url', stateCall, true)
			webview.removeEventListener('will-navigate', stateCall, true)
			webview.removeEventListener('did-navigate-in-page', stateCall, true)
			webview.removeEventListener('did-start-loading', didStartLoading, true)
			webview.removeEventListener('did-stop-loading', didStopLoading, true)

			webview.addEventListener('update-target-url', stateCall, true)
			webview.addEventListener('will-navigate', stateCall, true)
			webview.addEventListener('did-navigate-in-page', stateCall, true)
			webview.addEventListener('did-start-loading', didStartLoading, true)
			webview.addEventListener('did-stop-loading', didStopLoading, true)

			BrowserWindow.getCurrentWebContents().session.on('will-download', willDownload)

			if( localStorage.adminConnect === undefined ) {
 		 	if( self_.queryState != null ) self_.queryState.off()
 			 self_.queryState = firebase.database().ref(`${session}/host_reg/${uidhost}`)
 			 self_.queryState.on('value', snap => {
 				 let r = snap.val()
 				 console.log('Info: State events update - ',new Date().toISOString());
 				 if( r == null) {
 					 setTimeout(function(){ self.$store.commit( 'showFormLogin' ) },100)
 					 core.notify( "Expired session data", "warning" )
 				 }if( r.active === false ) {
 					 self.$store.commit( 'showFormLogin' )
 					 core.notify( "Your account is disabled", "info" )
 				 }if(r.active === true ) {
 					 self.$store.commit( 'showPluginWeb' )
 				 }if( r.reload === true ) {
 					 console.log('Info: State Reload location - ',new Date().toISOString());
					 r.reload = false
					 self_.queryState.set(r)
					 console.log(self.urlLogout)
					 self_.logoutPlugin()
 					 location.reload()
 				 }
 			 })
 		 }

		localStorage.linksession = session
	 },

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 15/8/2018
	 * Desc: borramos los elementos.
	 */
	adblock: function (list_adblock) {
		let bb = JSON.stringify( list_adblock.adblock );
		let webview = document.querySelector( 'webview' );
		webview.executeJavaScript( `
    var bloqueadores = ${bb};
    var links = document.querySelectorAll('*');
    links.forEach(function(a){
      bloqueadores.forEach(function(b){
        let data = b.split(':');
        if(data.length == 1){ if(document.querySelector(data[0]) != undefined)document.querySelector(data[0]).remove(); }
        else if(a.localName == data[0] && a[data[1]].indexOf(data[2]) != -1){ if(a.parentNode != undefined)a.parentNode.remove(); }
      });
    });` );
	},

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 16/8/2018
	 * Desc: si el plugin esta conectado lo
	 *			 desconectanos.
	 */
	logoutPlugin: function (c) {
		console.log(c);
		return;
		core.loadUrl(localStorage.logouturl, function ( webview ) {
			webview.loadURL( 'http://google.com' );
			localStorage.clear();
			console.log(typeof c);
			if(typeof c === 'function')c()
		});
	},

	/*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 21/8/2018
	 * Desc: buscamos un plugin activo.
	 */
	 searchPluginActive: function (session,  c ) {
		 let read = firebase.database().ref(session+'/plugins');
		 read.once('value', read_fbsa => {
			 let fbsa_result = read_fbsa.val()

			 let r = fbsa_result.filter(a => a.active == "1")
			 if( r.length === 0 ) {
				 core.notify("The plugin is not configured","warning")
				 c('disable')
			 } else {
				 	this.plugin = r[0]
					localStorage.logouturl = r[0].urllogout
				  c(r[0] )
			 }

		 })
	 },

	 /*
		* Author: hbiblia@gmail.com - slam
		* Fecha: 21/8/2018
		* Desc: conectamos con el plugin activado.
		*/
		activeConnect: function (session, callback) {
			this.searchPluginActive(session, (r) => {
				if( r==='disable' ){ callback('disable'); return false; }
				this.session = session
				this.loginFormConnect((result)=>{
					callback(result)
				})
			})
		},

		/*
 		* Author: hbiblia@gmail.com - slam
 		* Fecha: 22/8/2018
 		* Desc: cargamos la url del plugin.
 		*/
		loginFormConnect: function (callback) {
			let plugin = this.plugin
			core.loadUrl(`https://${plugin.urllogin}`, () => {
				if(this.completedautentificacion == false)
					console.log('Info: Plugin autentificación - %s',new Date().toISOString());
				else
					console.log('Info: Plugin autentificación [ok] - %s',new Date().toISOString());

				/* verificamos donde estamos antes de hacer el login. */
				this.checkLocation(plugin,(r) => {
					if( r === true ) {
						core.loadUrl(plugin.url, () => {
							callback('completed')
						})
					} else {
						core.exec(`
							document.querySelector('#login-username').value="${plugin.usr}",
							document.querySelector('#login-password').value="${plugin.pwd}",
							document.querySelector('#login').click()
							`, () => {
								core.navigate(()=>{
									if( this.maxIntent <= this.intentLogin ){
										core.notify('Too many attempts to authenticate the plugin','warning')
										callback('max-intent')
										return false
									}
									if( document.querySelector('#messages') != undefined ) {
										core.notify('An error occurred in the authentication of the plugin','error')
										callback('error-login-plugin')
										return false
									}
									this.completedautentificacion = true
									this.intentLogin
									this.loginFormConnect(callback)
								 })
						})
					}
				})
			})
		},

		/*
 		* Author: hbiblia@gmail.com - slam
 		* Fecha: 22/8/2018
 		* Desc: checkLocation
 		*/
		checkLocation: function (plugin, callback) {
			core.exec(`window.location`, (r) => {
				callback(r.pathname === plugin.pathname)
			})
		}
}
