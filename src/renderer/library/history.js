
/*
 * Author: hbiblia@gmail.com - slam
 * Desc: manejamos todo lo relacionado al historial.
 */

import os from 'os'
import crypto from 'crypto'

export default {

  /*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 15/8/2018
   * Desc: registramos la descarga.
   */
  save: function (id) {
    let session = localStorage.linksession
    let time = new Date().toLocaleString('en-US');
    let name_id = crypto.createHash( 'md5' ).update(id+time).digest( "hex" );

    /* registramos el historial */
    firebase.database().ref(session+'/history/'+name_id).set({
      user: os.userInfo().username,
      host: os.hostname(),
      platform: os.platform(),
      id: id,
      date: time
    });
  },

  /*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 15/8/2018
   * Desc: obtenemos los datos del history.
   */
  load: function(history) {
    let userId = firebase.auth().currentUser.uid;
    let query_c = firebase.database().ref(`${userId}/history/`);
    query_c.on( 'value', snapshot => {
      history(snapshot.val());
    });
  },

  /*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 15/8/2018
   * Desc: contamos cuantas descargas tiene un usuario.
   */
  getDownloadUsers: function(data, user) {
    let contador = 0;
    for(var name in data) {
      contador += (data[name].user === user)
    }return contador;
  }
}
