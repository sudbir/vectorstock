
/*
 * Author: hbiblia@gmail.com - slam
 * Desc: manejamos todo lo relacionado al login.
 */

import os from 'os'
import crypto from 'crypto'
import publicIp from 'public-ip'
import core from './core'
import swal from 'sweetalert2'

export default {

  /*
	 * Author: hbiblia@gmail.com - slam
	 * Fecha: 14/8/2018
	 * Desc: generamos un iud del host
	 */
	getUidHost: function () {
		let platform = os.platform();
		let hostname = os.hostname();
		let usuario = os.userInfo().username;
		return crypto.createHash( 'md5' ).update( hostname + platform + usuario ).digest( "hex" );
	},

  /*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 20/8/2018
   * Desc:
   */
  disengageHost: function () {
    localStorage.clear();
  },

	/*
   * Author: hbiblia@gmail.com - slam
   * Fecha: 21/8/2018
	 * Update: 8/10/2018
   * Desc: registramos el  host aceeptado
   */
  registerHost: function (uid, c) {
		let uid_ = uid.split('-')

		publicIp.v4().then(ip => {
			let platform = os.platform()
			let hostname = os.hostname()
			let username = os.userInfo().username

	    let firebase_ = firebase.database().ref(uid+'/host_reg/'+this.getUidHost())
	    firebase_.set({
				ip: ip,
				date:core.getDate(),
				reload:false,
				active:true,
				host:hostname,
				username:username,
				platform: platform,
				uid: uid
			}).then((r) => {
				console.log(r)
			})
			if (typeof c === 'function') c()
		})
  }
}
