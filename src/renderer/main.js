import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import store from './store'
import axios from 'axios'

import toolbarmain from './components/toolbar.vue'
import login from './components/login.vue'
import panel from './components/panel.vue'

Vue.component( 'toolbarmain', toolbarmain );
Vue.component( 'login', login );
Vue.component( 'panel', panel );

Vue.use( Vuex )

var config = {
	apiKey: "AIzaSyBV2ydWpnC2LYHBg7ThoAM5zZsKNv8P1R0",
	authDomain: "vectorstock-88b79.firebaseapp.com",
	databaseURL: "https://vectorstock-88b79.firebaseio.com",
	projectId: "vectorstock-88b79",
	storageBucket: "vectorstock-88b79.appspot.com",
	messagingSenderId: "126253786190"
};
firebase.initializeApp( config );

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

document.ondrop = document.ondragover = function (e) { e.preventDefault() }

/* eslint-disable no-new */
new Vue({
  components: { App },
  store,
  template: '<App/>'
}).$mount('#app')
